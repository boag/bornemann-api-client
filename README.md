# Bornemann API Client

```javascript
const Client = require('bornemann-api-client')
const client = new Client({appkey: ‘dauerhafte API Key vom Nutzer’})
const client = new Client({sessionId: ‘vom bisherigen Session gespeicherte Session ID vom Cookie’})
const client = new Client()
const client = new Client({baseUrl: ‘https://minx.fluorine.mygink.net/’’})
Alle Funktionen sind Promise-basiert!
client.getSessionId()
client.getAppKey()
client.ensureAppKey() [wird ein appkey erstellt, falls keinen vorhanden ist]
client.login(username, password)
client.getUserData()
client.getHardware() → client.get(hardwareUrl)
client.get(url)
client.put(url, body)
client.post(url, body)
client.delete(url)
```
