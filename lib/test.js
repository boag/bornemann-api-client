/* eslint-env mocha */
'use strict'

const debug = require('debug')('bornemann-api-client')
const should = require('should')
const CONF = require('convig').env({
  WSURL: 'http://localhost:3001/',
  WSUSER: 'demo',
  WSPASS: 'demo',
  WSPRIVAPPKEY: 'demosuper'
})
const Client = require('./index')
const UUID = require('uuid')

describe('bornemann-api-client', () => {
  let normalUser
  describe('normal user privs', () => {
    let sessionId
    it('should allow login on local service and appkey deletion', () => {
      const client = new Client({ baseUrl: CONF.WSURL })
      return client.login(CONF.WSUSER, CONF.WSPASS).then((res) => {
        res.statusCode.should.equal(200)
        res.body.uname.should.equal(CONF.WSUSER)
        res.body.should.have.property('hrefMe')
        sessionId = client.getSessionId()
        should.exist(sessionId)
        return client.get(res.body.hrefMe)
      }).then((res) => {
        res.statusCode.should.equal(200)
        res.body.should.have.property('href')
        return client.put(res.body.href, { appkey: '' })
      }).then((res) => {
        res.statusCode.should.equal(200)
        res.body.should.not.have.property('appkey')
      })
    })
    it('should allow session resumption for getUserData', () => {
      const client = new Client({ baseUrl: CONF.WSURL, sessionId: sessionId })
      should.exist(client.getSessionId())
      return client.getUserData().then((data) => {
        normalUser = data // saved for later test!
        data.uname.should.equal(CONF.WSUSER)
        should.exist(data.custNr)
      })
    })
    let appKey
    it('ensureAppKey should generate an appkey in user', () => {
      const client = new Client({ baseUrl: CONF.WSURL, sessionId: sessionId })
      return client.ensureAppKey().then((appkey) => {
        should.exist(appkey)
        appKey = appkey
      }).then(() => {
        return client.ensureAppKey()
      }).then((appkey) => {
        normalUser.appkey = appkey // saved for later test!
        appkey.should.equal(appKey)
      })
    })
    it('should allow client calls when using appkey', () => {
      const client = new Client({ baseUrl: CONF.WSURL, appkey: appKey })
      return client.getUserData().then((user) => {
        user.uname.should.equal(CONF.WSUSER)
      })
    })
  })
  describe('hardware registration privs', () => {
    let asset
    let hardware
    it('should be able to register hardware/asset pair', () => {
      const client = new Client({ baseUrl: CONF.WSURL, appkey: CONF.WSPRIVAPPKEY })
      const serialNumber = UUID.v4()
      const custNr = 'TESTME'
      const userId = normalUser._id
      const hwData = {
        make: 'migardo',
        model: 'migardo-app',
        serial: serialNumber,
        idPerson: userId,
        custNr: custNr
      }
      return client.registerHardware(hwData).then((data) => {
        asset = data.asset
        hardware = data.hardware
        asset.name.should.equal(serialNumber)
        hardware.make.should.equal(hwData.make)
        hardware.model.should.equal(hwData.model)
        hardware.serial.should.equal(hwData.serial)
        hardware.idAsset.should.equal(asset._id)
        hardware.custNr.should.equal(custNr)
        asset.custNr.should.equal(custNr)
      })
    })
    it('should allow normal client to get registered hardware/asset', () => {
      const client = new Client({ baseUrl: CONF.WSURL, appkey: normalUser.appkey })
      return client.getAllHardware().then((hwList) => {
        const hw = hwList.filter((h) => h._id === hardware._id)[0]
        should.exist(hw)
        hw.serial.should.equal(hardware.serial)
      })
    })
  })
  describe('poi creation', () => {
    let client
    before(() => {
      client = new Client({ baseUrl: CONF.WSURL, appkey: normalUser.appkey })
    })
    it('should be able to create a marker POI', () => {
      const data = {name: 'my marker POI', center: [10.643, 53.57]}
      return client.createPoi(data).then((poi) => {
        should.exist(poi)
        return client.deletePoi(poi)
      })
    })
    it('should be able to create/delete circular POI', () => {
      const data = {name: 'my circle POI', center: [10.643, 53.57], radius: 500}
      return client.createPoi(data).then((poi) => {
        should.exist(poi)
        return client.deletePoi(poi)
      })
    })
    it('should be able to create/delete fence POI', () => {
      const data = {
        name: 'my circle POI',
        fence: [
          [10.643, 53.57],
          [10.643, 55.57],
          [10.743, 55.57],
          [10.743, 53.57]
        ]
      }
      return client.createPoi(data).then((poi) => {
        should.exist(poi)
        return client.deletePoi(poi)
      })
    })
  })
})
