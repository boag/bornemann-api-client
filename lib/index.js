'use strict'

const debug = require('debug')('bornemann-api-client')
const crypto = require('crypto')
const Promise = require('bluebird')
const Request = require('request')

class Client {
  constructor (opts) {
    opts = opts || {}
    this.cachedUserData = null
    this.appkey = null
    this.baseUrl = (opts.baseUrl || 'https://api.bornemann.net').replace(/\/$/, '')
    this.jar = Request.jar()
    this.requester = Promise.promisifyAll(Request.defaults({
      jar: this.jar,
      json: true,
      headers: opts.headers || {}
    }))
    if (opts.appkey) { this.setAppKey(opts.appkey) }
    if (opts.sessionId) { this.setSessionId(opts.sessionId) }
  }
  get (uri) { return this.requester.getAsync({uri}) }
  put (uri, body) { return this.requester.putAsync({ uri, body }) }
  post (uri, body) { return this.requester.postAsync({ uri, body }) }
  delete (uri) { return this.requester.deleteAsync({ uri }) }
  _ensureSuccess (res, note) {
    if (res.statusCode < 200 || res.statusCode > 299) {
      note = note || ''
      throw new Error(`HTTP Error: ${res.statusCode} ${note}`)
    }
  }
  setAppKey (appkey) {
    this.appkey = appkey
    this._resetOptions({headers: {Authorization: 'Token ' + appkey}})
  }
  _resetOptions (opts) {
    this.requester = Promise.promisifyAll(this.requester.defaults(opts))
  }
  login (user, pass) {
    const url = this.baseUrl + '/auth/login'
    return this.post(url, {username: user, password: pass})
  }
  setSessionId (sid) {
    const cookie = Request.cookie('sid=' + sid)
    this.jar.setCookie(cookie, this.baseUrl)
  }
  getSessionId () {
    const cookies = this.jar.getCookies(this.baseUrl)
    let sid = null
    for (let cookie of cookies) {
      if (cookie.key === 'sid') { sid = cookie.value }
    }
    return sid
  }
  getUserData (noCache) {
    return Promise.try(() => {
      if (!noCache && this.cachedUserData) { return this.cachedUserData }
      return this.get(this.baseUrl + '/data/me').then((res) => {
        this._ensureSuccess(res, 'getting user data')
        if (res.body) { this.cachedUserData = res.body }
        return this.cachedUserData
      })
    })
  }
  getAppKey () { return this.appkey }
  ensureAppKey () {
    return this.getUserData(true).then((user) => {
      if (user && user.appkey) { return user.appkey }
      const appkey = crypto.randomBytes(30).toString('base64')
      return this.put(user.href, {appkey}).then((res) => res.body.appkey)
    }).then((appkey) => {
      this.setAppKey(appkey)
      return appkey
    })
  }
  getAllHardware () {
    return this.getUserData().then((user) => {
      return this.get(user.hrefHardware)
    }).then((res) => {
      this._ensureSuccess(res, 'getting Hardware metadata')
      return this.get(res.body.hrefAll)
    }).then((res) => {
      this._ensureSuccess(res, 'getting Hardware/all')
      return res.body
    })
  }
  registerHardware (data) {
    const ret = {hardware: {}, asset: {}}
    let user
    return this.getUserData().then((obj) => {
      user = obj
      const toPost = Object.assign({name: data.serial || ''}, data)
      return this.post(user.hrefAsset, toPost).then((res) => {
        this._ensureSuccess(res, 'creating Asset')
        const asset = res.body
        Object.assign(ret.asset, asset)
        if (!data.idPerson) { return asset }
        const body = { allowed: ['view', 'edit'], idPerson: data.idPerson }
        return this.post(asset.hrefAccess, body).then(() => asset)
      })
    }).then((asset) => {
      const toPost = Object.assign({}, data, {idAsset: asset._id})
      return this.post(user.hrefHardware, toPost).then((res) => {
        this._ensureSuccess(res, 'creating Hardware')
        const hw = res.body
        Object.assign(ret.hardware, hw)
        if (!data.idPerson) { return hw }
        const body = { allowed: ['view'], idPerson: data.idPerson }
        return this.post(hw.hrefAccess, body).then(() => hw)
      })
    }).then(() => ret)
  }
  createPoi (data) {
    return this.getUserData().then((user) => {
      return this.post(user.hrefPoi, data)
    }).then((res) => {
      this._ensureSuccess(res)
      return res.body
    })
  }
  deletePoi (obj) {
    return this.delete(obj.href).then((res) => {
      this._ensureSuccess(res)
      return res
    })
  }
  getAllPois () {
    return this.getUserData().then((user) => {
      return this.get(user.hrefPois)
    }).then((res) => {
      this._ensureSuccess(res, 'getting Poi metadata')
      return this.get(res.body.hrefAll)
    }).then((res) => {
      this._ensureSuccess(res, 'getting Poi/all')
      return res.body
    })
  }
}

module.exports = Client
